package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func addHeaders(rw http.ResponseWriter) {
	rw.Header().Add("Access-Control-Allow-Origin", "http://localhost:8080")
	rw.Header().Add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
	rw.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	rw.Header().Add("Access-Control-Allow-Credentials", "true")
}

func AllSchedulesHandler(rw http.ResponseWriter, req *http.Request) {
	addHeaders(rw)

	tx, err := DB.Begin()
	if err != nil {
		log.Printf("Error begining transaction: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	defer tx.Rollback()

	schedCursor, err := tx.Query("SELECT * FROM schedules")
	if err != nil {
		log.Printf("Error retrieving schedules: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}

	var scheds []*Schedule

	for schedCursor.Next() {
		sched := &Schedule{Dates: make([]*ScheduleDate, 0)}
		if err := schedCursor.Scan(&sched.ID, &sched.Name, &sched.Description); err != nil {
			log.Printf("Error scanning schedule: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		}
		datesCursor, err := tx.Query("SELECT id, date, description, need FROM schedule_dates WHERE schedule = ?", sched.ID)
		if err != nil {
			log.Printf("Error retrieving schedule dates: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		}
		for datesCursor.Next() {
			d := &ScheduleDate{}
			dateStr := ""
			if err := datesCursor.Scan(&d.ID, &dateStr, &d.Description, &d.Need); err != nil {
				log.Printf("Error scanning schedule date: %v", err)
				rw.WriteHeader(500)
				rw.Write([]byte("oops"))
				return
			}
			if date, err := time.Parse(dateFormat, dateStr); err != nil {
				log.Printf("Error parsing date: %v", err)
				rw.WriteHeader(500)
				rw.Write([]byte("oops"))
				return
			} else {
				st := SignupTime(date)
				d.Date = &st
			}
			sched.Dates = append(sched.Dates, d)
		}
		scheds = append(scheds, sched)
	}

	resp := &struct {
		Schedules []*Schedule `json:"schedules"`
	}{Schedules: scheds}
	body, err := json.Marshal(resp)
	if err != nil {
		log.Printf("Error encoding schedules: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	rw.WriteHeader(200)
	rw.Write(body)
}

func SchedulesPostHandler(rw http.ResponseWriter, req *http.Request) {
	addHeaders(rw)
	if req.Method == "OPTIONS" {
		rw.WriteHeader(200)
		return
	}

	if req.Body == nil {
		rw.WriteHeader(400)
		rw.Write([]byte("Expected JSON body"))
		return
	}

	sched := &Schedule{}
	if err := json.NewDecoder(req.Body).Decode(sched); err != nil {
		rw.WriteHeader(400)
		rw.Write([]byte(err.Error()))
		return
	}
	sched.ID = fmt.Sprintf("%x", rand.Int63())

	tx, err := DB.Begin()
	if err != nil {
		log.Printf("Error starting transaction: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	_, err = tx.Exec(
		"INSERT INTO schedules (id, name, description) VALUES (?, ?, ?)",
		sched.ID, sched.Name, sched.Description,
	)
	if err != nil {
		log.Printf("Error inserting schedule: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		tx.Rollback()
		return
	}

	for _, sd := range sched.Dates {
		sd.ID = fmt.Sprintf("%x", rand.Int63())
		_, err := tx.Exec(
			"INSERT INTO schedule_dates (schedule, id, date, description, need) VALUES (?, ?, ?, ?, ?)",
			sched.ID, sd.ID, time.Time(*sd.Date).Format(dateFormat), sd.Description, sd.Need,
		)
		if err != nil {
			log.Printf("Error inserting schedule date: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			tx.Rollback()
			return
		}
	}

	if err := tx.Commit(); err != nil {
		log.Printf("Error commit schedule: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		tx.Rollback()
		return
	}
	req.URL.RawQuery = ""
	req.URL.Fragment = ""
	rw.Header().Add("Location", req.URL.String()+"/"+sched.ID)
	rw.WriteHeader(201)
	json.NewEncoder(rw).Encode(struct {
		ScheduleID string `json:"schedule_id"`
	}{sched.ID})
	return
}

func GetScheduleHandler(rw http.ResponseWriter, req *http.Request) {
	addHeaders(rw)

	vars := mux.Vars(req)
	id := vars["id"]

	tx, err := DB.Begin()
	if err != nil {
		log.Printf("Error begining transaction: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	defer tx.Rollback()

	row := tx.QueryRow("SELECT * FROM schedules WHERE id = ?", id)

	sched := &Schedule{Dates: make([]*ScheduleDate, 0)}
	if err := row.Scan(&sched.ID, &sched.Name, &sched.Description); err != nil {
		log.Printf("Error scanning schedule: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	datesCursor, err := tx.Query("SELECT id, date, description, need FROM schedule_dates WHERE schedule = ?", sched.ID)
	if err != nil {
		log.Printf("Error retrieving schedule dates: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	for datesCursor.Next() {
		d := &ScheduleDate{}
		dateStr := ""
		if err := datesCursor.Scan(&d.ID, &dateStr, &d.Description, &d.Need); err != nil {
			log.Printf("Error scanning schedule date: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		}
		if date, err := time.Parse(dateFormat, dateStr); err != nil {
			log.Printf("Error parsing date: %v", err)
			rw.WriteHeader(500)
			rw.Write([]byte("oops"))
			return
		} else {
			st := SignupTime(date)
			d.Date = &st
		}
		sched.Dates = append(sched.Dates, d)
	}

	body, err := json.Marshal(sched)
	if err != nil {
		log.Printf("Error encoding schedules: %v", err)
		rw.WriteHeader(500)
		rw.Write([]byte("oops"))
		return
	}
	rw.WriteHeader(200)
	rw.Write(body)
}
