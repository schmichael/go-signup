package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const dateFormat = "01/02/2006"

type SignupTime time.Time

func (t *SignupTime) UnmarshalJSON(p []byte) error {
	if len(p) < 2 {
		return nil
	}
	rawT, err := time.Parse(dateFormat, string(p[1:len(p)-1]))
	*t = SignupTime(rawT)
	return err
}

func (t *SignupTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + t.String() + `"`), nil
}

func (t SignupTime) String() string {
	return time.Time(t).Format(dateFormat)
}

type Schedule struct {
	ID          string          `json:"id"`
	Name        string          `json:"name"`
	Description string          `json:"description"`
	Dates       []*ScheduleDate `json:"dates"`
}

type ScheduleDate struct {
	ID          string      `json:"id"`
	Date        *SignupTime `json:"date"`
	Description string      `json:"description"`
	Need        int         `json:"need"`
	ScheduleID  string      `json:"schedule_id"`
}

var DB *sql.DB

func createTable(table, create string) {
	create = fmt.Sprintf(create, table)

	tx, err := DB.Begin()
	if err != nil {
		log.Fatalf("Error beginning transaction: %v", err)
	}

	row := tx.QueryRow("SELECT COUNT(1) FROM sqlite_master WHERE type = ? AND name = ?", "table", table)
	count := 0
	if err := row.Scan(&count); err != nil {
		log.Fatalf("Error reading results of '%s' table check: %v", table, err)
	}

	if count > 0 {
		tx.Rollback()
		return
	}
	if _, err := tx.Exec(create); err != nil {
		log.Fatalf("Error creating table %s: %v", table, err)
	}
	if err := tx.Commit(); err != nil {
		log.Fatalf("Error commiting table creation %s: %v", table, err)
	}
}

func initDB() {
	createTable("schedules", "CREATE TABLE %s ( id TEXT PRIMARY KEY, name TEXT, description TEXT )")

	createTable("schedule_dates", "CREATE TABLE %s ( id TEXT PRIMARY KEY, schedule TEXT, date TEXT, need INTEGER, description TEXT, FOREIGN KEY(schedule) REFERENCES schedules(id) )")

	createTable("schedule_signups", "CREATE TABLE %s ( id TEXT PRIMARY KEY, schedule_date TEXT, person TEXT, FOREIGN KEY(schedule_date) REFERENCES schedule_dates(id) )")
}

func main() {
	bind := flag.String("bind", "localhost:8000", "host:port to listen on")
	dbFile := flag.String("db", "signup.sqlite3", "filename for db")
	flag.Parse()

	router, err := setup(*dbFile)
	if err != nil {
		log.Fatal("Error starting up: %v", err)
	}
	m := http.NewServeMux()
	m.Handle("/", router)

	log.Printf("Starting %s on http://%s", path.Base(os.Args[0]), *bind)
	log.Fatal(http.ListenAndServe(*bind, m))
}

func setup(dbFile string) (*mux.Router, error) {
	var err error
	if DB, err = sql.Open("sqlite3", dbFile); err != nil {
		return nil, fmt.Errorf("Error opening database: %v", err)
	}

	initDB()

	r := mux.NewRouter()
	r.HandleFunc("/api/schedules", AllSchedulesHandler).Methods("GET")
	r.HandleFunc("/api/schedules", SchedulesPostHandler).Methods("POST", "OPTIONS")
	r.HandleFunc("/api/schedules/{id}", GetScheduleHandler).Methods("GET", "OPTIONS")

	return r, nil
}
