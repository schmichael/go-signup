Signup API
==========

See https://gitlab.com/pietvanzoen/signup

Installation
------------

.0. SQLite

You'll need SQLite's development files installed before continuing.

.1. Install Go:

Read [Go's documentation](http://golang.org/doc/install) or use a system specific method:

**OSX with Brew**

```
brew install go
```

**Ubuntu or Debian**

```
sudo apt-get install golang
```

.2. Set `$GOPATH`

On Linux, OSX, or other UNIXes, create a directory to hold Go files and
binaries. This will be your `$GOPATH`.

```
mkdir ~/go
export GOPATH=$HOME/go  # Place this line in your ~/.bashrc or ~/.bash_profile
```

.3. go install

Finally install go-signup:

```
go get -u -v bitbucket.org/schmichael/go-signup
# -u is for update, so if you run this again it will update your copy
# -v is for verbose, so you can see what's going on
```

Run the command at any time to get the latest version.

.4. Starting the API

```
cd $GOPATH/bin
./go-signup
```

You can stop the API by pressing `CTRL-C`.

Update it by running Step 3 again.

Using the API
-------------

```sh
echo '{
    "name": "foo",
    "description": "this is the foo",
    "dates": [
        {
            "date": "12/30/2014",
            "description": "first foo",
            "need":5
        }
    ]
}' | http POST localhost:8000/api/schedules

http localhost:8000/api/schedules  # Bask in the glory of your scheduled event
```
